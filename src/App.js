import styles from './App.module.css';
import Main from "./pages/Main/Main";

function App() {
  return (
    <div className={styles.App}>
        <Main/>
    </div>
  );
}

export default App;
