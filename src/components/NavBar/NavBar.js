import React, {useState, useRef} from 'react';
import styles from './navBar.module.css';
import {useDispatch, useSelector} from "react-redux";
import {handleUserSearchInput} from "../../store/slices/itemSlice";

function NavBar() {
    const searchBarRef = useRef(null);
    const [displaySearchBar, setDisplaySearchBar] = useState(false);
    const searchFilters = useSelector((state) => state.searchFilters);

    const dispatch = useDispatch();
    const handleDisplaySearchBar = () => {
        if(!displaySearchBar){
            setDisplaySearchBar(() => true)
        } else {
            dispatch(handleUserSearchInput(searchBarRef.current.value));
        }
    }

    const handleEnterKey = (e) => {
        if(e.key === 'Enter'){
            dispatch(handleUserSearchInput(searchBarRef.current.value));
        }
    }

    const tmp = () => {
        console.log(searchBarRef.current.value)
        dispatch(handleUserSearchInput(searchBarRef.current.value));
    }

    return (
        <div className={styles.navBarContainer}>
            <img src={"./icons/sideroLogo.png"} alt={"Sidero logo"} className={styles.appLogo}/>
            <span className={styles.spacer}/>
            <div className={styles.searchBarContainer}>
                <input className={
                    displaySearchBar ? styles.searchBar + ' ' +styles.searchBarExpanded : styles.searchBar + ' ' +styles.searchBarClosed
                } type="text" placeholder="Search item" onKeyDown={handleEnterKey} ref={searchBarRef} onChange={tmp}/>
            </div>
            <button className={styles.searchButton} onClick={handleDisplaySearchBar}>
                <img className={styles.searchButtonIcon} src={"./icons/searchIcon.png"} alt={"Search icon"}/>
            </button>
        </div>
    );
}

export default NavBar;