import React from 'react';

import classes from './Select.module.css';

const Select = React.forwardRef((props, ref) => {
  return (
    <div className={classes.select}>
      <label htmlFor={props.select.id}>{props.label}</label>
      <select ref={ref} id ={props.select.id}>
      {
         props.select.options.map((category, index) => {
         return <option value={category} key={index}>{category}</option>
       })
     }
          </select>
    </div>
  );
});

export default Select;