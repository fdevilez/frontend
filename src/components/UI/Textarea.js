import React from "react";

import classes from "./Textarea.module.css";

const Textarea = React.forwardRef((props, ref) => {
  return (
    <div className={classes.textarea}>
      <label htmlFor={props.textarea.id}>{props.label}</label>
      <textarea ref={ref} {...props.textarea} />
    </div>
  );
});

export default Textarea;
