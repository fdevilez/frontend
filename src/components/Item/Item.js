import React from 'react';
import styles from './item.module.css';
import axios from "axios";

function Item({id, title, description, price, image}) {
    const handleBuyItem = async () => {
        const axiosConfig = {
            method: 'patch',
            url: `http://localhost:4444/sidero/item/${id}`,
            data: {
                bought: false
            }
        }
        try {
            await axios(axiosConfig);
        } catch (e) {
            console.log(e)
        }
    }

    return (
        <div className={styles.itemContainer}>
            <div className={styles.picturePlaceHolder}>

            </div>
            <div className={styles.itemInfo}>
                <p>{title}</p>
                <p>{description}</p>
                <div className={styles.priceButton}>
                    <p>€{price}</p>
                    <button className={styles.button} onClick={handleBuyItem}>Buy</button>
                </div>
            </div>
        </div>
    );
}

export default Item;