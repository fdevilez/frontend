import React, {useEffect, useRef, useState} from 'react';
import Input from '../UI/Input';
import Select from '../UI/Select';
import Modal from '../UI/Modal';
import styles from './selldialog.module.css';
import axios from "axios";


const SellDialog = (props) => {
    const[image,setImage] = useState(null)
    const [categories, setCategories] = useState(["cat1", "cat2", "cat3", "cat4", "cat5"])
    const titleInputRef = useRef();
    const descriptionInputRef = useRef();
    const amountInputRef = useRef();
    const imageInputRef = useRef();
    const imageViewInputRef = useRef();
    const categoryInputRef = useRef();

    const imageChangeHandler = (e)=>{
      e.preventDefault();
      if(e.target.files && e.target.files[0]){
        let reader = new FileReader();
        reader.onload = function(ev){
          setImage(ev.target.result);
          //imageViewInputRef.src = ev.target.result;
        }.bind(this);
        reader.readAsDataURL(e.target.files[0]);
      }
    }

    const submitHandler =  (event) => {
        event.preventDefault();

        const enteredTitle = titleInputRef.current.value;
        const enteredDescription = descriptionInputRef.current.value;
        const enteredAmount = amountInputRef.current.value;
        const enteredImage = imageInputRef.current.value;

        console.log(enteredTitle, enteredDescription, enteredAmount,enteredImage)
        props.onClose();
    }

    const handleGetCategories = async () => {
        const axiosConfig = {
            url: `http://localhost:4444/sidero/categories`,
            header: {},
            method: 'get'
        };
        const response = await axios(axiosConfig);
        return response.data
    }

    useEffect(() => {
        handleGetCategories().then((response) => {
            console.log(response);
            setCategories(response.map((category) => category.category))
        })
    }, [])

    return (
        <Modal onClose={props.onClose} title={'Sell Item'}>
         <div className={styles.popUpContainer}>
         <form className={styles.form} onSubmit={submitHandler}>
             <Input ref={titleInputRef}  label='Title' input={{id: 'title', type: 'string', defaultValue: '' }}></Input>
             <Input ref={descriptionInputRef} label='Description' input={{id: 'description',type: 'string',defaultValue: ''}}></Input>
             <Input ref={imageInputRef} label='Image' input={{id: 'image', type: 'file', defaultValue: '', accept:"image/*", onChange: imageChangeHandler}}
             ></Input> 
             <div className = {styles.imageViewer}>
             {image!==null &&<img ref={imageViewInputRef} src={image}/>}
             </div>
            <Input ref={amountInputRef} label='Price' input={{id: 'price', type: 'number', defaultValue: ''}}></Input>
            <Select ref={categoryInputRef} label='Category' select = {{id: 'category', options:categories}}></Select>
             <div className = {styles.sellButton}>
             <button>Sell</button>
             </div>
             </form>
         </div>
        </Modal>
    );
}

export default SellDialog;