import React, {useEffect, useRef, useState} from 'react';
import styles from './sidebar.module.css';
import {useDispatch, useSelector} from "react-redux";
import {handleGlobalSearch} from "../../store/slices/itemSlice";
import axios from "axios";

function SideBar(props) {
    const [categories, setCategories] = useState(["cat1", "cat2", "cat3", "cat4", "cat5"])
    const minPriceInputRef = useRef(null);
    const maxPriceInputRef = useRef(null);
    const categoryInputRef = useRef(null);
    const dispatch = useDispatch();
    const searchFilters = useSelector((state) => state.searchFilters);

    const handleFiltering = () => {
        const minPrice = minPriceInputRef.current.value;
        const maxPrice = maxPriceInputRef.current.value;
        const category = categoryInputRef.current.value;
        dispatch(handleGlobalSearch({
            minPrice,
            maxPrice,
            category
        }))
    }
    const handleSellClick = ()=>{
        console.log('sell clicked')
    }

    const handleGetCategories = async () => {
        const axiosConfig = {
            url: `http://localhost:4444/sidero/categories`,
            header: {},
            method: 'get'
        };
        const response = await axios(axiosConfig);
        return response.data
    }

    useEffect(() => {
        handleGetCategories().then((response) => {
            console.log(response);
            setCategories(response.map((category) => category.category))
        })
    }, [])

    return (
        <div className={styles.sideBarContainer}>
            <div className={styles.sideBarTop}>
                <button className={styles.button} onClick={props.OnSellClick}><span>Sell</span></button>
                 <hr className={styles.divider}/>
            </div>
            <div className={styles.filteringOption}>
                <p className={styles.sideBarTextItem}>Price range</p>
                <div className={styles.priceFilteringInputsContainer}>
                    <input className={styles.priceFilteringInputItem} ref={minPriceInputRef} placeholder={"min"}/>
                    <input className={styles.priceFilteringInputItem} ref={maxPriceInputRef} placeholder={"max"}/>
                </div>
                <p className={styles.sideBarTextItem}>Category</p>
                <select ref={categoryInputRef} className={styles.dropDown}>
                    {
                        categories.map((category, index) => {
                            return <option value={category} key={index}>{category}</option>
                        })
                    }
                </select>
                <button onClick={handleFiltering} className={styles.searchButton + ' ' + styles.button}>Filter items</button>
            </div>
        </div>
    );
}

export default SideBar;