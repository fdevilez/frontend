import React, {useEffect,useState} from 'react';
import styles from './main.module.css';
import NavBar from "../../components/NavBar/NavBar";
import SideBar from "../../components/SideBar/SideBar";
import {handleFetchItems} from "../../store/slices/itemSlice";
import {useDispatch, useSelector} from "react-redux";
import Item from "../../components/Item/Item";
import PopUp from '../../components/SellDialog/SellDialog';

function Main() {
    const dispatch = useDispatch();
    const items = useSelector((state) => {
        return state.item.items
    });

    useEffect(() => {
        dispatch(handleFetchItems());
    }, [handleFetchItems, dispatch])

      const [dialogShown, setDialogShownIsShown] = useState(false);

    const showDialogHandler = () => {
        setDialogShownIsShown(true);
    };
  
    const hideDialogHandler = () => {
        setDialogShownIsShown(false);
    };
    return (
        <div className={styles.MainPageContainer}>
            {dialogShown && <PopUp onClose={hideDialogHandler}></PopUp>}
            <NavBar/>
            <div className={styles.content}>
                <SideBar OnSellClick = {showDialogHandler}/>

                <div className={styles.itemList}>
                    {
                        items && items.length > 0 ? items.map((item, index) => {
                            return <Item
                                id={item._id}
                                title={item.title}
                                description={item.description}
                                price={item.price}
                                image={item.image}
                                key={index}
                            />
                        }) : <p>No items found</p>
                    }
                </div>
            </div>
        </div>
    );
}

export default Main;