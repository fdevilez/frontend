import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
    items: [],
    searchFilters: {
        textInput: '',
        minPrice: 0,
        maxPrice: 9999,
        category: ''
    }
}

const itemSlice = createSlice({
    name: 'item',
    initialState,
    reducers: {
        handleUserSearchInput: (state, action) => {
            state.searchFilters.textInput = action.payload;
        },
        handleUserFilteringInput: (state, action) => {
            state.searchFilters.category = action.payload.category;
            state.searchFilters.minPrice = action.payload.minPrice;
            state.searchFilters.maxPrice = action.payload.maxPrice;
        }
    },
    extraReducers: builder => {
        builder.addCase(handleFetchItems.fulfilled, (state, action) => {
            state.items = action.payload;
        });
        builder.addCase(handleGlobalSearch.fulfilled, (state, action) => {
            console.log(action)
            console.log(state)
            state.items = action.payload;
        });
    }
})

export const handleFetchItems = createAsyncThunk(
    '/items',
    async () => {
        const axiosConfig = {
            method: 'get',
            url: 'http://localhost:4444/sidero/items'
        };
        const response = await axios(axiosConfig);
        return response.data;
    }
)

export const handleGlobalSearch = createAsyncThunk(
    '/items/searchGlobal',
    async ({minPrice, maxPrice, category}, {getState}) => {
        const state = getState();
        const queryParams = new URLSearchParams({
            minPrice: minPrice ? minPrice : undefined,
            maxPrice: maxPrice ? maxPrice : undefined,
            category: category ? category : undefined,
            title: state.item.searchFilters.textInput ? state.item.searchFilters.textInput : ''
        });
        const axiosConfig = {
            url: `http://localhost:4444/sidero/items/search?${queryParams}`,
            header: {},
            method: 'get'
        };
        try{
            const response = await axios(axiosConfig);
            if(response && response.data){
                return response.data;
            }
        } catch (e){
            return []
        }
        return [];
    }
)

export default itemSlice.reducer;
export const {handleUserSearchInput, handleUserFilteringInput} = itemSlice.actions;