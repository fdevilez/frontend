import {configureStore} from "@reduxjs/toolkit";
import itemSlice from "./slices/itemSlice";

const store = configureStore({
    reducer: {
        item: itemSlice
    }
})

export default store;
export const dispatch = store.dispatch;